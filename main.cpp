#include <Windows.h>
#include <iostream>
#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "glext.h"
#include "glpomoc.h"
#include "colors.h"
#include "materials.h"
#include "biblioteka.h"
using namespace std;

#define LICZBA_OB_TEXTUR 2
GLuint obiektyTextur[LICZBA_OB_TEXTUR];
const char* plikiTextur[LICZBA_OB_TEXTUR] = { ".//rock.tga", ".//stone.tga" };

enum Wybor
{
	SWIATLO_ON,
	REFLEKTOR,
	SWIATLO_OFF,
	PERSPTKYWA,
	ORTHO,
	MGLA,
	TRYB_MGLY,
	PRZYWROC,
	ASPECT_1_1,
	EXIT
};

bool isReflektorOn = false;
bool isMglaOn = false;

GLPMatrix Macierz_cieni;
GLPMatrix Macierz_cieni2;
GLfloat fKolor_cienia[] = { 0.0f,0.0f,0.0f,0.0f };
GLPVector3 vPoints[3] = { {0.0f, -1.9f,  0.0f},
						 {10.0f, -1.9f,  0.0f},
						  {5.0f, -1.9f, -5.0f} };
GLPVector3 vPoints2[3] = { {0.0f, -1.5f,  0.0f},
						  {10.0f, -1.5f,  0.0f},
						   {5.0f, -1.5f, -5.0f} };
GLPVector3 bezCienia[3] = { {0.0f, -50.5f,  0.0f},
						   {10.0f, -50.5f,  0.0f},
						   { 5.0f, -50.5f, -5.0f} };

GLfloat zakres = 1.0f;
GLfloat blisko = 2.0f;
GLfloat daleko = 50.0f;

static void display(void);
int rzut = PERSPTKYWA;
int skala = ASPECT_1_1;
int xx, yy, zz, xz, xy, yx, yz, zx, zy, obrot_y, obrot_x;

GLfloat fPozycja_swiatla_pod[] = { 2.0f, -2.0f, 0.0f, 1.0f };
GLfloat fPozycja_swiatla[] = { 2.0f, 2.0f, 0.0f, 1.0f };

GLfloat fKierunek_reflektora[] = { 0.0f, 1.0f, -12.0f, 1.0f };
GLfloat fKierunek_reflektora_pod[] = { 0.0f, -1.5f, -12.0f, 1.0f };
GLfloat fPozycja_reflektora[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat fPozycja_reflektora_pod[] = { 0.0f, 0.0f, 0.0f, 1.0f };

double a = 0.0f;
GLfloat rozwarcie = 25.0;
GLfloat skupienie = 2.0f;

GLfloat przesunX = 0.0;
GLfloat przesunZ = 0.0;

GLfloat fSlabe_swiatlo[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat fMocne_swiatlo[] = { 1.0f, 1.0f, 1.0f, 1.0f };

GLfloat fSlabe_swiatloR[] = { 0.15f, 0.15f, 0.15f, 1.0f };
GLfloat fMocne_swiatloR[] = { 1.0f, 1.0f, 1.0f, 1.0f };

const GLfloat* ambient = BrassAmbient;
const GLfloat* diffuse = BrassDiffuse;
const GLfloat* specular = BrassSpecular;
const GLfloat shininess[] = { 0.5f, 0.5f, 0.5f, 1.0f };

void podloze()
{
	glBegin(GL_QUADS);
	glNormal3f(0.0, 1.0, 0.0);
	for (GLfloat z = -100.0; z < 100.0; z += 1)
	{
		for (GLfloat x = -100.0; x < 100.0; x += 1)
		{
			glColor4f(0.0, 0.0, 0.0, 0.5);
			glVertex3f(x + 0.0, 0.0, z + 0.0);
			glVertex3f(x + 0.0, 0.0, z + 0.5);
			glVertex3f(x + 0.5, 0.0, z + 0.5);
			glVertex3f(x + 0.5, 0.0, z + 0.0);
			glColor4f(1, 1, 1, 0.5);
			glVertex3f(x + 0.5, 0.0, z + 0.0);
			glVertex3f(x + 0.5, 0.0, z + 0.5);
			glVertex3f(x + 1.0, 0.0, z + 0.5);
			glVertex3f(x + 1.0, 0.0, z + 0.0);
			glColor4f(0.0, 0.0, 0.0, 0.5);
			glVertex3f(x + 0.5, 0.0, z + 0.5);
			glVertex3f(x + 0.5, 0.0, z + 1.0);
			glVertex3f(x + 1.0, 0.0, z + 1.0);
			glVertex3f(x + 1.0, 0.0, z + 0.5);
			glColor4f(1, 1, 1, 0.5);
			glVertex3f(x + 0.0, 0.0, z + 0.5);
			glVertex3f(x + 0.0, 0.0, z + 1.0);
			glVertex3f(x + 0.5, 0.0, z + 1.0);
			glVertex3f(x + 0.5, 0.0, z + 0.5);
		}
	}
	glEnd();
}

GLfloat light_position[4] = { 0.0, 0.0, 2.0, 0.0 };

GLfloat light_rotatex = 0.0;
GLfloat light_rotatey = 0.0;

GLint fog_hint = GL_NICEST;
GLfloat fog_start = 10.0;
GLfloat fog_width = 5.0;
GLfloat fog_end = fog_start + fog_width;
GLfloat fog_density = 0.3;
GLfloat fog_mode = GL_LINEAR;
GLfloat color_mgly = 0.2;
bool mgla = true;

#ifdef near
#undef near
#endif
#ifdef far
#undef far
#endif

GLfloat rotatex = 0.0;
GLfloat rotatey = 0.0;

const GLdouble left = -2.0;
const GLdouble right = 2.0;
const GLdouble bottom = -2.0;
const GLdouble top = 2.0;
const GLdouble near = 3.0;
const GLdouble far = 7.0;

PFNGLWINDOWPOS2IPROC glWindowPos2i = NULL;

int predkoscObrotu = 1;

static void Ksztalt(bool ktora, bool cien)
{
	glPushMatrix();

	glColor4f(0.5f, 0.5f, 0.5f, 0.6f);
	if (ktora)
		glTranslated(0 + przesunX, 1, -13 + przesunZ);
	else
		glTranslated(0 + przesunX, 6, -13 + przesunZ);
	glRotated(0, a, 0, 1);
	glRotatef(obrot_x, 0.0f, 1.0f, 0.0f);
	glRotatef(obrot_y, xy, yy, zy);

	glRotatef(rotatex, 1.0, 0.0, 0.0);
	glRotatef(rotatey, 0.0, 1.0, 0.0);

	if (cien)
		glColor4f(Black[0], Black[1], Black[2], 0.2f);

	if (cien) {
		glNormal3f(0.0, 0.0, 1.0);

		GLUquadric* qobj = gluNewQuadric();
		gluSphere(qobj, 1, 50, 12);
		glEnd();
	}
	else {
		glColor4f(White[0], White[1], White[2], 1.0f);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, obiektyTextur[1]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

		GLUquadric* qobj = gluNewQuadric();
		gluQuadricTexture(qobj, GL_TRUE);
		gluSphere(qobj, 1, 50, 12);

		gluDeleteQuadric(qobj);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	glPopMatrix();
}

static void resize(int width, int height)
{
	const float ar = (float)width / (float)height;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-ar, ar, -1.0, 1.0, 2.0, 50.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Reshape(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (rzut == PERSPTKYWA)
	{
		if (skala == ASPECT_1_1)
		{
			if (width < height && width > 0)
				glFrustum(-zakres, zakres, -zakres * height / width, zakres * height / width, blisko, daleko);
			else
				if (width >= height && height > 0)
					glFrustum(-zakres * width / height, zakres * width / height, -zakres, zakres, blisko, daleko);
		}
		else
			glFrustum(-zakres, zakres, -zakres, zakres, blisko, daleko);
	}
	else if (rzut == ORTHO) {
		if (skala == ASPECT_1_1)
		{
			if (width < height && width > 0)
				glOrtho(-15.0, 15.0, 2.0, 15.0 * height / width, -30.0, 30.0);
			else
				if (width >= height && height > 0)
					glOrtho(-4.0 * width / height, 4.0 * width / height, -4.0, 4.0, -30.0, 30.0);
		}
		else
			glOrtho(-15.0, 15.0, -5.0, 15.0, -30.0, 30.0);
	}
	display();
}

static void przywracanie()
{
	zakres = 1.0f;
	blisko = 1.5f;
	daleko = 50.0f;
}

void Menu(int value)
{
	switch (value)
	{
	case EXIT:
		exit(0);
	case SWIATLO_ON:
		isReflektorOn = false;
		glDisable(GL_LIGHT1);
		glEnable(GL_LIGHT0);
		glDisable(GL_LIGHT3);
		glEnable(GL_LIGHT2);

		break;
	case REFLEKTOR:
		isReflektorOn = true;
		glDisable(GL_LIGHT0);
		glEnable(GL_LIGHT1);
		glDisable(GL_LIGHT2);
		glEnable(GL_LIGHT3);
;
		break;
	case SWIATLO_OFF:
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHT1);
		glDisable(GL_LIGHT2);
		glDisable(GL_LIGHT3);
		break;

	case ASPECT_1_1:
		skala = ASPECT_1_1;
		przywracanie();
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;
	case PERSPTKYWA:
		rzut = PERSPTKYWA;
		przywracanie();
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;
	case PRZYWROC:
		rzut = PERSPTKYWA;
		skala = ASPECT_1_1;
		przywracanie();
		resize(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;
	case MGLA:
		isMglaOn = !isMglaOn;
		break;
	case TRYB_MGLY:
		if (fog_mode == GL_LINEAR)
			fog_mode = GL_EXP2;
		else
			fog_mode = GL_LINEAR;
		break;
	case ORTHO:
		rzut = ORTHO;
		przywracanie();
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;
	}
	display();
}
void WylaczSwiatlo() {
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
}

void WlaczBlendowanie() {
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
}

void WlaczSwiatlo() {
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
}

void RdzenOdbicie() {
	glPushMatrix();
	glFrontFace(GL_CW);
	glTranslated(0, 0.85, 0);
	glScalef(1.0f, -1.0f, 1.0f);
	glTranslated(0, 0.85, 0);
	Ksztalt(false, false);
	glTranslated(0, -0.85, 0);
	glFrontFace(GL_CCW);
	glPopMatrix();
}

void RysujPodloge() {
	glPushMatrix();
	glTranslated(0, -2, 0);
	if (isReflektorOn)
		WlaczSwiatlo();
	else
		WylaczSwiatlo();
	podloze();
	glPopMatrix();
}

void RdzenCien() {
	glPushMatrix();
	WylaczSwiatlo();
	glMultMatrixf(Macierz_cieni);
	glColor4f(0, 0, 0, 1);
	Ksztalt(true, true);
	glPopMatrix();
}

void RdzenTextura() {
	glPushMatrix();
	Ksztalt(true, false);
	glPopMatrix();
}

void ExtensionSetup()
{
	const char* version = (char*)glGetString(GL_VERSION);
	int major = 0, minor = 0;
	if (sscanf(version, "%d.%d", &major, &minor) != 2)
	{
        #ifdef WIN32
		printf("B��dny format wersji OpenGL\n");
        #else
		printf("Bledny format wersji OpenGL\n");
        #endif
		exit(0);
	}

	if (major > 1 || minor >= 4)
	{
		glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress("glWindowPos2i");
	}
	else
		if (glutExtensionSupported("GL_ARB_window_pos"))
		{
			glWindowPos2i = (PFNGLWINDOWPOS2IPROC)wglGetProcAddress
			("glWindowPos2iARB");
		}
		else
		{
			printf("Brak rozszerzenia ARB_window_pos!\n");
			exit(0);
		}
}

void RysujMgle() {
	glEnable(GL_FOG);
	glHint(GL_FOG_HINT, fog_hint);
	glFogfv(GL_FOG_COLOR, White);
	glFogf(GL_FOG_DENSITY, fog_density);
	glFogf(GL_FOG_MODE, fog_mode);
	glFogf(GL_FOG_START, fog_start);
	glFogf(GL_FOG_END, fog_end);
}

void SzescianSolid(const double time) {
	glPushMatrix();
	glTranslated(2, 1, -6);
	glRotated(0, a, 0, 1);
	glRotatef(obrot_x, 0.0f, 1.0f, 0.0f);
	glRotatef(obrot_y, xy, yy, zy);
	glColor4f(1, 1, 1, 1.0f);
	glPushMatrix();
	glutSolidCube(1.5);
	glPopMatrix();
	glPopMatrix();
	glFlush();
}

static void display(void)
{
	const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	if (isMglaOn)
		RysujMgle();
	else {
		glDisable(GL_FOG);
	}

	WlaczSwiatlo();

	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	RdzenOdbicie();

	WlaczSwiatlo();

	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(1, 1, 1, 0.75);
	RysujPodloge();
	RdzenCien();
	WlaczSwiatlo();
	RdzenTextura();
	SzescianSolid(t);
	glLightfv(GL_LIGHT0, GL_POSITION, fPozycja_swiatla);
	glLightfv(GL_LIGHT1, GL_POSITION, fPozycja_reflektora);
	glEnable(GL_DEPTH_TEST);
	WlaczSwiatlo();
	glFlush();
	glutSwapBuffers();
}

static void key(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
	case 'q':
		exit(0);
		break;
	case 'd':
	{
		fPozycja_swiatla[0] -= 1.0f;
		fPozycja_swiatla_pod[0] -= 1.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, fPozycja_swiatla);
		glLightfv(GL_LIGHT2, GL_POSITION, fPozycja_swiatla_pod);
		display();
	}
	break;
	case 'a':
	{
		fPozycja_swiatla[0] += 1.0f;
		fPozycja_swiatla_pod[0] += 1.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, fPozycja_swiatla);
		glLightfv(GL_LIGHT2, GL_POSITION, fPozycja_swiatla_pod);
		display();
	}
	break;
	case 's':
	{
		fPozycja_swiatla[1] += 1.0f;
		fPozycja_swiatla_pod[1] -= 1.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, fPozycja_swiatla);
		glLightfv(GL_LIGHT2, GL_POSITION, fPozycja_swiatla_pod);
		display();
	}
	break;
	case 'w':
	{
		fPozycja_swiatla[1] -= 1.0f;
		fPozycja_swiatla_pod[1] += 1.0f;
		glLightfv(GL_LIGHT0, GL_POSITION, fPozycja_swiatla);
		glLightfv(GL_LIGHT2, GL_POSITION, fPozycja_swiatla_pod);
		display();
	}
	break;
	case 'h':
	{
		fPozycja_reflektora[0] -= 0.1f;
		fPozycja_reflektora_pod[0] += 0.1f;
		glLightfv(GL_LIGHT1, GL_POSITION, fPozycja_reflektora);
		glLightfv(GL_LIGHT3, GL_POSITION, fPozycja_reflektora_pod);
		display();
	}
	break;
	case 'f':
	{
		fPozycja_reflektora[0] += 0.1f;
		fPozycja_reflektora_pod[0] -= 0.1f;
		glLightfv(GL_LIGHT1, GL_POSITION, fPozycja_reflektora);
		glLightfv(GL_LIGHT3, GL_POSITION, fPozycja_reflektora_pod);
		display();
	}
	break;
	case 'g':
	{
		fPozycja_reflektora[1] += 0.1f;
		fPozycja_reflektora_pod[1] -= 0.1f;
		glLightfv(GL_LIGHT1, GL_POSITION, fPozycja_reflektora);
		glLightfv(GL_LIGHT3, GL_POSITION, fPozycja_reflektora_pod);
		display();
	}
	break;
	case 't':
	{
		fPozycja_reflektora[1] -= 0.1f;
		fPozycja_reflektora_pod[1] += 0.1f;
		glLightfv(GL_LIGHT1, GL_POSITION, fPozycja_reflektora);
		glLightfv(GL_LIGHT3, GL_POSITION, fPozycja_reflektora_pod);
		display();
	}
	break;

	case '+':
	{
		rozwarcie += 1.0;
		glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, rozwarcie);
		glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, rozwarcie);
		display();
	}
	break;
	case '-':
	{
		rozwarcie -= 1.0;
		glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, rozwarcie);
		glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, rozwarcie);
		display();
	}
	break;
	case '*':
	{
		skupienie += 1.0;
		glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, skupienie);
		glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, skupienie);
		display();
	}
	break;
	case '/':
	{
		skupienie -= 1.0;
		glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, skupienie);
		glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, skupienie);
		display();
	}
	break;
	case 'u':
		rotatey -= 2.0;
		break;
	case 'o':
		rotatey += 2.0;
		break;
	case 'l':
	{
		xx = 0;
		yx = 1;
		zx = 0;
		obrot_x = obrot_x + 5;
	}
	break;
	case 'j':
	{
		xx = 0;
		yx = 1;
		zx = 0;
		obrot_x = obrot_x - 5;
	}
	break;
	case 'k':
	{
		xy = 1;
		yy = 0;
		zy = 0;
		obrot_y = obrot_y + 5;
	}
	break;
	case 'i':
	{
		xy = 1;
		yy = 0;
		zy = 0;
		obrot_y = obrot_y - 5;
	}
	break;
	}
	glutPostRedisplay();

}

static void idle(void)
{
	glutPostRedisplay();
}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitWindowSize(700, 500);
	glutInitWindowPosition(10, 10);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Projekt Krystian Lech");

	glEnable(GL_TEXTURE_2D);
	glGenTextures(LICZBA_OB_TEXTUR, obiektyTextur);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	for (int i = 0; i < LICZBA_OB_TEXTUR; i++)
	{
		glBindTexture(GL_TEXTURE_2D, obiektyTextur[i]);
	}
	glDisable(GL_TEXTURE_2D);
	glutReshapeFunc(Reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(key);
	glutIdleFunc(idle);

	glClearColor(0.9, 1, 0.6, 0);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_LIGHTING);
	glDisable(GL_LIGHT0);   //o gora
	glDisable(GL_LIGHT1);   //r gora
	glDisable(GL_LIGHT2);   //o dol
	glDisable(GL_LIGHT3);   //r dol

	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	glLightfv(GL_LIGHT0, GL_AMBIENT, fSlabe_swiatlo);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, fMocne_swiatlo);
	glLightfv(GL_LIGHT0, GL_SPECULAR, fMocne_swiatlo);
	glLightfv(GL_LIGHT0, GL_POSITION, fPozycja_swiatla);

	glLightfv(GL_LIGHT2, GL_AMBIENT, fSlabe_swiatlo);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, fMocne_swiatlo);
	glLightfv(GL_LIGHT2, GL_SPECULAR, fMocne_swiatlo);
	glLightfv(GL_LIGHT2, GL_POSITION, fPozycja_swiatla_pod);

	glLightfv(GL_LIGHT1, GL_AMBIENT, fSlabe_swiatloR);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, fMocne_swiatloR);
	glLightfv(GL_LIGHT1, GL_SPECULAR, fMocne_swiatloR);
	glLightfv(GL_LIGHT1, GL_POSITION, fPozycja_reflektora);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, fKierunek_reflektora);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, rozwarcie);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, skupienie);

	glLightfv(GL_LIGHT3, GL_AMBIENT, fSlabe_swiatloR);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, fMocne_swiatloR);
	glLightfv(GL_LIGHT3, GL_SPECULAR, fMocne_swiatloR);
	glLightfv(GL_LIGHT3, GL_POSITION, fPozycja_reflektora_pod);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, fKierunek_reflektora_pod);
	glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, rozwarcie);
	glLightf(GL_LIGHT3, GL_SPOT_EXPONENT, skupienie);

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);

	glutCreateMenu(Menu);
	glutAddMenuEntry("Swiatlo ogolne", SWIATLO_ON);
	glutAddMenuEntry("Reflektor", REFLEKTOR);
	glutAddMenuEntry("Wylacz wszystkie swiatla", SWIATLO_OFF);
	glutAddMenuEntry("Wlacz / Wylacz mgle", MGLA);
	glutAddMenuEntry("Perspektywa", PERSPTKYWA);
	glutAddMenuEntry("Ortho", ORTHO);
	glutAddMenuEntry("Tryb mgly", TRYB_MGLY);
	glutAddMenuEntry("Wyjscie", EXIT);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	ExtensionSetup();

	glutMainLoop();

	return EXIT_SUCCESS;
}
